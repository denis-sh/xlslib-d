/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * This file is part of xlslib -- A multiplatform, C/C++ library
 * for dynamic generation of Excel(TM) files.
 *
 * Copyright 2008-2011 David Hoerl All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY David Hoerl ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL David Hoerl OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * File description:
 *
 *
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module xlslib;

public import xlslib.formula_const;

/*
#define CPP_BRIDGE_XLS

// since xlslib.h does not include these for C files
#include "common/xlsys.h"

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#include <string>

#include "common/systype.h"
#include "xlslib/common.h"
#include "xlslib/record.h"

#include "xlslib.h"

using namespace xlslib_core;
using namespace xlslib_strings;
*/

import core.stdc.wchar_;

struct workbook;
struct worksheet;
struct font_t;
struct format_t;
struct xf_t;
struct cell_t;
struct formula_t;
struct range;

alias unichar_t = wchar_t;

enum
{
	NO_ERRORS        = 0,
	FILE_ERROR       = -1,
	GENERAL_ERROR    = -100,
}

enum property_t
{
	author = 1,
	category,
	comments,
	company,
	creatingApplication,   // cannot see anywhere this is displayed [dfh: need the enum to increase by 1]
	keywords,
	manager,
	revision,
	subject,
	title,

	last
};

enum errcode_t
{
	null_  = 0x00, // #NULL!
	div0   = 0x07, // #DIV/0!
	value  = 0x0F, // #VALUE!
	ref_   = 0x17, // #REF!
	name   = 0x1D, // #NAME?
	num    = 0x24, // #NUM!
	n_a    = 0x2A, // #N/A!
	// since Excel 2010:
	gettingdata = 0x2B, // #DATA!
}
/*
enum : errcode_t
{
	XLERR_NULL  = 0x00, // #NULL!
	XLERR_DIV0  = 0x07, // #DIV/0!
	XLERR_VALUE = 0x0F, // #VALUE!
	XLERR_REF   = 0x17, // #REF!
	XLERR_NAME  = 0x1D, // #NAME?
	XLERR_NUM   = 0x24, // #NUM!
	XLERR_N_A   = 0x2A, // #N/A!
	// since Excel 2010:
	XLERR_GETTINGDATA = 0x2B, // #DATA!
}*/

enum : property_t
{
	PROP_AUTHOR                = property_t.author,
	PROP_CATEGORY			   = property_t.category,
	PROP_COMMENTS			   = property_t.comments,
	PROP_COMPANY			   = property_t.company,
	PROP_CREATINGAPPLICATION   = property_t.creatingApplication,
	PROP_KEYWORDS			   = property_t.keywords,
	PROP_MANAGER			   = property_t.manager,
	PROP_REVISION			   = property_t.revision,
	PROP_SUBJECT			   = property_t.subject,
	PROP_TITLE				   = property_t.title,
							   
	PROP_LAST				   = property_t.last
}


enum expr_operator_code_t
{
	OP_EXP = 0x01,                                 // ptgExp          01h   control
	OP_TBL = 0x02,                                 // ptgTbl          02h   control
	OP_ADD = 0x03,                                 // ptgAdd          03h   operator
	OP_SUB = 0x04,                                 // ptgSub          04h   operator
	OP_MUL = 0x05,                                 // ptgMul          05h   operator
	OP_DIV = 0x06,                                 // ptgDiv          06h   operator
	OP_POWER = 0x07,                               // ptgPower        07h   operator
	OP_CONCAT = 0x08,                              // ptgConcat       08h   operator
	OP_LT = 0x09,                                  // ptgLT           09h   operator
	OP_LE = 0x0A,                                  // ptgLE           0Ah   operator
	OP_EQ = 0x0B,                                  // ptgEQ           0Bh   operator
	OP_GE = 0x0C,                                  // ptgGE           0Ch   operator
	OP_GT = 0x0D,                                  // ptgGT           0Dh   operator
	OP_NE = 0x0E,                                  // ptgNE           0Eh   operator
	OP_ISECT = 0x0F,                               // ptgIsect        0Fh   operator
	OP_UNION = 0x10,                               // ptgUnion        10h   operator
	OP_RANGE = 0x11,                               // ptgRange        11h   operator
	OP_UPLUS = 0x12,                               // ptgUplus        12h   operator
	OP_UMINUS = 0x13,                              // ptgUminus       13h   operator
	OP_PERCENT = 0x14,                             // ptgPercent      14h   operator
	OP_PAREN = 0x15,                               // ptgParen        15h   control
	OP_MISSARG = 0x16,                             // ptgMissArg      16h   operand
	OP_STR = 0x17,                                 // ptgStr          17h   operand
	OP_ATTR = 0x19,                                // ptgAttr         19h   control
	OP_SHEET = 0x1A,                               // ptgSheet        1Ah   (ptg DELETED)
	OP_ENDSHEET = 0x1B,                            // ptgEndSheet     1Bh   (ptg DELETED)
	OP_ERR = 0x1C,                                 // ptgErr          1Ch   operand
	OP_BOOL = 0x1D,                                // ptgBool         1Dh   operand
	OP_INT = 0x1E,                                 // ptgInt          1Eh   operand
	OP_NUM = 0x1F,                                 // ptgNum          1Fh   operand
	OP_ARRAY = 0x20,                               // ptgArray        20h   operand, reference class
	OP_FUNC = 0x21,                                // ptgFunc         21h   operator
	OP_FUNCVAR = 0x22,                             // ptgFuncVar      22h   operator
	OP_NAME = 0x23,                                // ptgName         23h   operand, reference class
	OP_REF = 0x24,                                 // ptgRef          24h   operand, reference class
	OP_AREA = 0x25,                                // ptgArea         25h   operand, reference class
	OP_MEMAREA = 0x26,                             // ptgMemArea      26h   operand, reference class
	OP_MEMERR = 0x27,                              // ptgMemErr       27h   operand, reference class
	OP_MEMNOMEM = 0x28,                            // ptgMemNoMem     28h   control
	OP_MEMFUNC = 0x29,                             // ptgMemFunc      29h   control
	OP_REFERR = 0x2A,                              // ptgRefErr       2Ah   operand, reference class
	OP_AREAERR = 0x2B,                             // ptgAreaErr      2Bh   operand, reference class
	OP_REFN = 0x2C,                                // ptgRefN         2Ch   operand, reference class
	OP_AREAN = 0x2D,                               // ptgAreaN        2Dh   operand, reference class
	OP_MEMAREAN = 0x2E,                            // ptgMemAreaN     2Eh   control
	OP_MEMNOMEMN = 0x2F,                           // ptgMemNoMemN    2Fh   control
	OP_NAMEX = 0x39,                               // ptgNameX        39h   operand, reference class
	OP_REF3D = 0x3A,                               // ptgRef3d        3Ah   operand, reference class
	OP_AREA3D = 0x3B,                              // ptgArea3d       3Bh   operand, reference class
	OP_REFERR3D = 0x3C,                            // ptgRefErr3d     3Ch   operand, reference class
	OP_AREAERR3D = 0x3D,                           // ptgAreaErr3d    3Dh   operand, reference class
	OP_ARRAYV = 0x40,                              // ptgArrayV       40h   operand, value class
	OP_FUNCV = 0x41,                               // ptgFuncV        41h   operator
	OP_FUNCVARV = 0x42,                            // ptgFuncVarV     42h   operator
	OP_NAMEV = 0x43,                               // ptgNameV        43h   operand, value class
	OP_REFV = 0x44,                                // ptgRefV         44h   operand, value class
	OP_AREAV = 0x45,                               // ptgAreaV        45h   operand, value class
	OP_MEMAREAV = 0x46,                            // ptgMemAreaV     46h   operand, value class
	OP_MEMERRV = 0x47,                             // ptgMemErrV      47h   operand, value class
	OP_MEMNOMEMV = 0x48,                           // ptgMemNoMemV    48h   control
	OP_MEMFUNCV = 0x49,                            // ptgMemFuncV     49h   control
	OP_REFERRV = 0x4A,                             // ptgRefErrV      4Ah   operand, value class
	OP_AREAERRV = 0x4B,                            // ptgAreaErrV     4Bh   operand, value class
	OP_REFNV = 0x4C,                               // ptgRefNV        4Ch   operand, value class
	OP_AREANV = 0x4D,                              // ptgAreaNV       4Dh   operand, value class
	OP_MEMAREANV = 0x4E,                           // ptgMemAreaNV    4Eh   control
	OP_MEMNOMEMNV = 0x4F,                          // ptgMemNoMemNV   4Fh   control
	OP_FUNCCEV = 0x58,                             // ptgFuncCEV      58h   operator
	OP_NAMEXV = 0x59,                              // ptgNameXV       59h   operand, value class
	OP_REF3DV = 0x5A,                              // ptgRef3dV       5Ah   operand, value class
	OP_AREA3DV = 0x5B,                             // ptgArea3dV      5Bh   operand, value class
	OP_REFERR3DV = 0x5C,                           // ptgRefErr3dV    5Ch   operand, value class
	OP_AREAERR3DV = 0x5D,                          // ptgAreaErr3dV   5Dh   operand, value class
	OP_ARRAYA = 0x60,                              // ptgArrayA       60h   operand, array class
	OP_FUNCA = 0x61,                               // ptgFuncA        61h   operator
	OP_FUNCVARA = 0x62,                            // ptgFuncVarA     62h   operator
	OP_NAMEA = 0x63,                               // ptgNameA        63h   operand, array class
	OP_REFA = 0x64,                                // ptgRefA         64h   operand, array class
	OP_AREAA = 0x65,                               // ptgAreaA        65h   operand, array class
	OP_MEMAREAA = 0x66,                            // ptgMemAreaA     66h   operand, array class
	OP_MEMERRA = 0x67,                             // ptgMemErrA      67h   operand, array class
	OP_MEMNOMEMA = 0x68,                           // ptgMemNoMemA    68h   control
	OP_MEMFUNCA = 0x69,                            // ptgMemFuncA     69h   control
	OP_REFERRA = 0x6A,                             // ptgRefErrA      6Ah   operand, array class
	OP_AREAERRA = 0x6B,                            // ptgAreaErrA     6Bh   operand, array class
	OP_REFNA = 0x6C,                               // ptgRefNA        6Ch   operand, array class
	OP_AREANA = 0x6D,                              // ptgAreaNA       6Dh   operand, array class
	OP_MEMAREANA = 0x6E,                           // ptgMemAreaNA    6Eh   control
	OP_MEMNOMEMNA = 0x6F,                          // ptgMemNoMemNA   6Fh   control
	OP_FUNCCEA = 0x78,                             // ptgFuncCEA      78h   operator
	OP_NAMEXA = 0x79,                              // ptgNameXA       79h   operand, array class (NEW ptg)
	OP_REF3DA = 0x7A,                              // ptgRef3dA       7Ah   operand, array class (NEW ptg)
	OP_AREA3DA = 0x7B,                             // ptgArea3dA      7Bh   operand, array class (NEW ptg)
	OP_REFERR3DA = 0x7C,                           // ptgRefErr3dA    7Ch   operand, array class (NEW ptg)
	OP_AREAERR3DA = 0x7D,                          // ptgAreaErr3dA   7Dh   operand, array class (NEW ptg)
};


enum
{
	// The font-record field offsets:
	FORMAT_OFFSET_INDEX       = 4,
	FORMAT_OFFSET_NAMELENGTH  = 6,
	FORMAT_OFFSET_NAME        = 7,

	FMTCODE_GENERAL          = 0x0000,
	FMTCODE_NUMBER1          = 0x0001,
	FMTCODE_NUMBER2          = 0x0002,
	FMTCODE_NUMBER3          = 0x0003,
	FMTCODE_NUMBER4          = 0x0004,
	FMTCODE_CURRENCY1        = 0x0005,
	FMTCODE_CURRENCY2        = 0x0006,
	FMTCODE_CURRENCY3        = 0x0007,
	FMTCODE_CURRENCY4        = 0x0008,
	// CURRENCY ends up 'customized' in XLS
	FMTCODE_PERCENT1         = 0x0009,
	FMTCODE_PERCENT2         = 0x000a,
	FMTCODE_SCIENTIFIC1      = 0x000b,
	FMTCODE_FRACTION1        = 0x000c,
	FMTCODE_FRACTION2        = 0x000d,
	FMTCODE_DATE1            = 0x000e,
	FMTCODE_DATE2            = 0x000f,
	FMTCODE_DATE3            = 0x0010,
	FMTCODE_DATE4            = 0x0011,
	FMTCODE_HOUR1            = 0x0012,
	FMTCODE_HOUR2            = 0x0013,
	FMTCODE_HOUR3            = 0x0014,
	FMTCODE_HOUR4            = 0x0015,
	FMTCODE_HOURDATE         = 0x0016,
	FMTCODE_ACCOUNTING1      = 0x0025,
	FMTCODE_ACCOUNTING2      = 0x0026,
	FMTCODE_ACCOUNTING3      = 0x0027,
	FMTCODE_ACCOUNTING4      = 0x0028,
	FMTCODE_CURRENCY5        = 0x0029,
	FMTCODE_CURRENCY6        = 0x002a,
	FMTCODE_CURRENCY7        = 0x002b,
	FMTCODE_CURRENCY8        = 0x002c,
	// CURRENCY ends up 'customized' in XLS
	FMTCODE_HOUR5            = 0x002d,
	FMTCODE_HOUR6            = 0x002e,
	FMTCODE_HOUR7            = 0x002f,
	FMTCODE_SCIENTIFIC2      = 0x0030,
	FMTCODE_TEXT             = 0x0031,

	FMT_CODE_FIRST_USER      = 164, /* 0xA4 - the first index used by Excel2003 for any user defined format */
}

// good resource for format strings: http://www.mvps.org/dmcritchie/excel/formula.htm
// Good explanation of custom formats: http://www.ozgrid.com/Excel/CustomFormats.htm
// MS examples (need Windows): http://download.microsoft.com/download/excel97win/sample/1.0/WIN98Me/EN-US/Nmbrfrmt.exe
// Google this for MS help: "Create or delete a custom number format"
enum format_number_t
{
	FMT_GENERAL = 0,
	FMT_NUMBER1,            // 0
	FMT_NUMBER2,            // 0.00
	FMT_NUMBER3,            // #,##0
	FMT_NUMBER4,            // #,##0.00
	FMT_CURRENCY1,          // "$"#,##0_);("$"#,##0)
	FMT_CURRENCY2,          // "$"#,##0_);[Red]("$"#,##0)
	FMT_CURRENCY3,          // "$"#,##0.00_);("$"#,##0.00)
	FMT_CURRENCY4,          // "$"#,##0.00_);[Red]("$"#,##0.00)
	FMT_PERCENT1,           // 0%
	FMT_PERCENT2,           // 0.00%
	FMT_SCIENTIFIC1,        // 0.00E+00
	FMT_FRACTION1,          // # ?/?
	FMT_FRACTION2,          // # ??/??
	FMT_DATE1,              // M/D/YY
	FMT_DATE2,              // D-MMM-YY
	FMT_DATE3,              // D-MMM
	FMT_DATE4,              // MMM-YY
	FMT_TIME1,              // h:mm AM/PM
	FMT_TIME2,              // h:mm:ss AM/PM
	FMT_TIME3,              // h:mm
	FMT_TIME4,              // h:mm:ss
	FMT_DATETIME,           // M/D/YY h:mm
	FMT_ACCOUNTING1,        // _(#,##0_);(#,##0)
	FMT_ACCOUNTING2,        // _(#,##0_);[Red](#,##0)
	FMT_ACCOUNTING3,        // _(#,##0.00_);(#,##0.00)
	FMT_ACCOUNTING4,        // _(#,##0.00_);[Red](#,##0.00)
	FMT_CURRENCY5,          // _("$"* #,##0_);_("$"* (#,##0);_("$"* "-"_);_(@_)
	FMT_CURRENCY6,          // _(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)
	FMT_CURRENCY7,          // _("$"* #,##0.00_);_("$"* (#,##0.00);_("$"* "-"??_);_(@_)
	FMT_CURRENCY8,          // _(* #,##0.00_);_(* (#,##0.00);_(* "-"??_);_(@_)
	FMT_TIME5,              // mm:ss
	FMT_TIME6,              // [h]:mm:ss
	FMT_TIME7,              // mm:ss.0
	FMT_SCIENTIFIC2,        // ##0.0E+0
	FMT_TEXT                // @
}

// extformat.h:

enum fill_option_t
{
	FILL_NONE = 0,
	FILL_SOLID,
	FILL_ATEN75,
	FILL_ATEN50,
	FILL_ATEN25,
	FILL_ATEN12,
	FILL_ATEN06,
	FILL_HORIZ_LIN,
	FILL_VERTICAL_LIN,
	FILL_DIAG,
	FILL_INV_DIAG,
	FILL_INTER_DIAG,
	FILL_DIAG_THICK_INTER,
	FILL_HORIZ_LINES_THIN,
	FILL_VERTICAL_LINES_THIN,
	FILL_DIAG_THIN,
	FILL_INV_DIAG_THIN,
	FILL_HORIZ_INT_THIN,
	FILL_HORIZ_INTER_THICK,
	_NUM_FILL_OPTIONS
}

enum border_style_t
{
	BORDER_NONE = 0,
	BORDER_THIN,
	BORDER_MEDIUM,
	BORDER_DASHED,
	BORDER_DOTTED,
	BORDER_THICK,
	BORDER_DOUBLE,
	BORDER_HAIR,
	_NUM_BORDER_STYLES
}

enum border_side_t
{
	BORDER_BOTTOM = 0,
	BORDER_TOP,
	BORDER_LEFT,
	BORDER_RIGHT,
	DIAGONALS,      // BIFF8
	_NUM_BORDERS
}

enum halign_option_t
{
	HALIGN_GENERAL = 0,
	HALIGN_LEFT,
	HALIGN_CENTER,
	HALIGN_RIGHT,
	HALIGN_FILL,
	HALIGN_JUSTIFY,
	HALIGN_CENTERACCROSS,
	_NUM_HALIGN_OPTIONS
}

enum valign_option_t
{
	VALIGN_TOP = 0,
	VALIGN_CENTER,
	VALIGN_BOTTOM,
	VALIGN_JUSTIFY,
	_NUM_VALIGN_OPTIONS
}

enum txtori_option_t
{
	ORI_NONE = 0,
	ORI_TOPBOTTOMTXT,
	ORI_90NOCLOCKTXT,
	ORI_90CLOCKTXT,
	_NUM_TXTORI_OPTIONS
}

enum indent_option_t
{
	INDENT_0 = 0,
	INDENT_1,
	INDENT_2,
	INDENT_3,
	INDENT_4,
	INDENT_5,
	INDENT_6,
	INDENT_7,
	INDENT_8,
	INDENT_9,
	INDENT_10,
	INDENT_11,
	INDENT_12,
	INDENT_13,
	INDENT_14,
	INDENT_15,
	INDENT_SHRINK2FIT,
	INDENT_L2R,
	INDENT_R2L,
	_NUM_INDENT_OPTIONS
}

enum color_name_t
{
	ORIG_COLOR_BLACK = 0,	// Well, to get the default fonts etc to use same value as Excel outputs

	// Excel top 40 colors
	CLR_BLACK = 1,  CLR_BROWN,       CLR_OLIVE_GREEN, CLR_DARK_GREEN,      CLR_DARK_TEAL,      CLR_DARK_BLUE,  CLR_INDIGO,     CLR_GRAY80,
	CLR_DARK_RED,   CLR_ORANGE,      CLR_DARK_YELLOW, CLR_GREEN,           CLR_TEAL,           CLR_BLUE,       CLR_BLUE_GRAY,  CLR_GRAY50,
	CLR_RED,        CLR_LITE_ORANGE, CLR_LIME,        CLR_SEA_GREEN,       CLR_AQUA,           CLR_LITE_BLUE,  CLR_VIOLET,     CLR_GRAY40,
	CLR_PINK,       CLR_GOLD,        CLR_YELLOW,      CLR_BRITE_GREEN,     CLR_TURQUOISE,      CLR_SKY_BLUE,   CLR_PLUM,       CLR_GRAY25,
	CLR_ROSE,       CLR_TAN,         CLR_LITE_YELLOW, CLR_LITE_GREEN,      CLR_LITE_TURQUOISE, CLR_PALE_BLUE,  CLR_LAVENDER,   CLR_WHITE,

	// Bottom 16 colors
	CLR_PERIWINKLE, CLR_PLUM2,       CLR_IVORY,       CLR_LITE_TURQUOISE2, CLR_DARK_PURPLE,     CLR_CORAL,     CLR_OCEAN_BLUE, CLR_ICE_BLUE,  
	CLR_DARK_BLUE2, CLR_PINK2,       CLR_YELLOW2,     CLR_TURQUOISE2,      CLR_VIOLET2,         CLR_DARK_RED2, CLR_TEAL2,      CLR_BLUE2,

	CLR_SYS_WIND_FG, CLR_SYS_WIND_BG,

	_NUM_COLOR_NAMES
}

enum boldness_option_t
{
	BOLDNESS_BOLD = 0,
	BOLDNESS_HALF,
	BOLDNESS_NORMAL,
	BOLDNESS_DOUBLE
}

enum script_option_t
{
	SCRIPT_NONE = 0,
	SCRIPT_SUPER,
	SCRIPT_SUB
}

enum underline_option_t
{
	UNDERLINE_NONE = 0,
	UNDERLINE_SINGLE,
	UNDERLINE_DOUBLE,
	UNDERLINE_SINGLEACC,
	UNDERLINE_DOUBLEACC
}


extern(C) nothrow:
alias xlslib_userdef_assertion_reporter_p = void function(const char* expr, const char* fname, int lineno, const char* funcname);

@nogc:

// Workbook
workbook* xlsNewWorkbook();
void xlsDeleteWorkbook(workbook* w);

worksheet* xlsWorkbookSheet(workbook* w, const char* sheetname);
worksheet* xlsWorkbookSheetW(workbook* w, const unichar_t* sheetname);
worksheet* xlsWorkbookGetSheet(workbook* w, ushort sheetnum);


font_t* xlsWorkbookFont(workbook* w, const char* name);
format_t* xlsWorkbookFormat(workbook* w, const char* name);
format_t* xlsWorkbookFormatW(workbook* w, const unichar_t* name);
xf_t* xlsWorkbookxFormat(workbook* w);
xf_t* xlsWorkbookxFormatFont(workbook* w, font_t* font);


version(HAVE_WORKING_ICONV)
int xlsWorkbookIconvInType(workbook* w, const char* inType);
ubyte xlsWorkbookProperty(workbook* w, property_t prop, const char* s);
void xlsWorkBookWindPosition(workbook* w, ushort horz, ushort vert);

void xlsWorkBookWindSize(workbook* w, ushort horz, ushort vert);

void xlsWorkBookFirstTab(workbook* w, ushort firstTab);

void xlsWorkBookTabBarWidth(workbook* w, ushort width);

int xlsWorkbookDump(workbook* w, const char* filename);
// Worksheet
void xlsWorksheetMakeActive(worksheet* w);
cell_t* xlsWorksheetFindCell(worksheet* w, uint row, uint col);

// Cell operations
void xlsWorksheetMerge(worksheet* w, uint first_row, uint first_col, uint last_row, uint last_col);

void xlsWorksheetColwidth(worksheet* w, uint col, ushort width, xf_t* pxformat);

void xlsWorksheetRowheight(worksheet* w, uint row, ushort height, xf_t* pxformat);

// Ranges
range* xlsWorksheetRangegroup(worksheet* w, uint row1, uint col1, uint row2, uint col2);


// Cells
cell_t* xlsWorksheetLabel(worksheet* w, uint row, uint col, const char* strlabel, xf_t* pxformat);
cell_t* xlsWorksheetLabelW(worksheet* w, uint row, uint col, const unichar_t* strlabel, xf_t* pxformat);
cell_t* xlsWorksheetBlank(worksheet* w, uint row, uint col, xf_t* pxformat);


cell_t* xlsWorksheetNumberDbl(worksheet* w, uint row, uint col, double numval, xf_t* pxformat);

// 536870911 >= numval >= -536870912
cell_t* xlsWorksheetNumberInt(worksheet* w, uint row, uint col, int numval, xf_t* pxformat);


cell_t* xlsWorksheetBoolean(worksheet* w, uint row, uint col, int boolval, xf_t* pxformat);


cell_t* xlsWorksheetError(worksheet* w, uint row, uint col, errcode_t errval, xf_t* pxformat);


cell_t* xlsWorksheetNote(worksheet* w, uint row, uint col, const char* remark, const char* author, xf_t* pxformat);
cell_t* xlsWorksheetNoteW(worksheet* w, uint row, uint col, const unichar_t* remark, const unichar_t* author, xf_t* pxformat);
formula_t* xlsWorksheetFormula(worksheet* w);
void xlsFormulaPushBoolean(formula_t* formula, bool value);
void xlsFormulaPushMissingArgument(formula_t* formula);
void xlsFormulaPushError(formula_t* formula, ubyte value);
void xlsFormulaPushNumberInt(formula_t* formula, int value);
void xlsFormulaPushNumberDbl(formula_t* formula, double value);
void xlsFormulaPushNumberArray(formula_t* formula, double* values, size_t count);
void xlsFormulaPushOperator(formula_t* formula, expr_operator_code_t op);
void xlsFormulaPushCellReference(formula_t* formula, cell_t* cell, cell_addr_mode_t opt);
void xlsFormulaPushCellAreaReference(formula_t* formula, cell_t* upper_left_cell,
        cell_t* lower_right_cell, cell_addr_mode_t opt);
void xlsFormulaPushFunction(formula_t* formula, expr_function_code_t func);
void xlsFormulaPushFunctionV(formula_t* formula, expr_function_code_t func, size_t arg_count);
void xlsFormulaPushCharacterArray(formula_t* formula, const char* text, size_t count);
void xlsFormulaPushCharacterArrayW(formula_t* formula, const unichar_t* text, size_t count);
void xlsFormulaPushText(formula_t* formula, const char* text);
void xlsFormulaPushTextW(formula_t* formula, const unichar_t* text);
void xlsFormulaPushTextArray(formula_t* formula, const char* *text, size_t count);
void xlsFormulaPushTextArrayW(formula_t* formula, const unichar_t* *text, size_t count);

cell_t* xlsWorksheetFormulaCell(worksheet* w, uint row, uint col, formula_t* formula, xf_t* pxformat);
void xlsWorksheetValidateCell(worksheet* w, cell_t* cell, uint options,
        const formula_t* cond1, const formula_t* cond2,
        const char* prompt_title, const char* prompt_text,
        const char* error_title, const char* error_text);
void xlsWorksheetValidateCellW(worksheet* w, cell_t* cell, uint options,
        const formula_t* cond1, const formula_t* cond2,
        const unichar_t* prompt_title, const unichar_t* prompt_text,
        const unichar_t* error_title, const unichar_t* error_text);
void xlsWorksheetValidateCellArea(worksheet* w, cell_t* upper_left_cell,
        cell_t* lower_right_cell, uint options,
        const formula_t* cond1, const formula_t* cond2,
        const char* prompt_title, const char* prompt_text,
        const char* error_title, const char* error_text);
void xlsWorksheetValidateCellAreaW(worksheet* w, cell_t* upper_left_cell,
        cell_t* lower_right_cell, uint options,
        const formula_t* cond1, const formula_t* cond2,
        const unichar_t* prompt_title, const unichar_t* prompt_text,
        const unichar_t* error_title, const unichar_t* error_text);
void xlsWorksheetHyperLink(worksheet* w, cell_t* cell, const char* url, const char* mark);
void xlsWorksheetHyperLinkW(worksheet* w, cell_t* cell, const unichar_t* url, const unichar_t* mark);

// Cells
// xf_i interface
void xlsCellFont(cell_t* c, font_t* fontidx);
void xlsCellFormat(cell_t* c, format_number_t format);
void xlsCellFormatP(cell_t* c, format_t* format);
void xlsCellHalign(cell_t* c, halign_option_t ha_option);
void xlsCellValign(cell_t* c, valign_option_t va_option);
void xlsCellIndent(cell_t* c, indent_option_t in_option);
void xlsCellOrientation(cell_t* c, txtori_option_t ori_option);

void xlsCellFillfgcolor(cell_t* c, color_name_t color);
void xlsCellFillbgcolor(cell_t* c, color_name_t color);
void xlsCellFillstyle(cell_t* c, fill_option_t fill);
void xlsCellLocked(cell_t* c, bool locked_opt);
void xlsCellHidden(cell_t* c, bool hidden_opt);
void xlsCellWrap(cell_t* c, bool wrap_opt);
void xlsCellBorderstyle(cell_t* c, border_side_t side, border_style_t style);

void xlsCellBordercolor(cell_t* c, border_side_t side, color_name_t color);

void xlsCellBordercolorIdx(cell_t* c, border_side_t side, ubyte color);

//font_i interface
void xlsCellFontname(cell_t* c, const char* fntname);
void xlsCellFontheight(cell_t* c, ushort fntheight);
void xlsCellFontbold(cell_t* c, boldness_option_t fntboldness);

void xlsCellFontunderline(cell_t* c, underline_option_t fntunderline);

void xlsCellFontscript(cell_t* c, script_option_t fntscript);
void xlsCellFontcolor(cell_t* c, color_name_t fntcolor);
void xlsCellFontitalic(cell_t* c, bool italic);
void xlsCellFontstrikeout(cell_t* c, bool so);
void xlsCellFontoutline(cell_t* c, bool ol);
void xlsCellFontshadow(cell_t* c, bool sh);

uint xlsCellGetRow(cell_t* c);
uint xlsCellGetCol(cell_t* c);
// range
void xlsRangeCellcolor(range* r, color_name_t color);
// xformat
void xlsXformatSetFont(xf_t* x, font_t* fontidx);
ushort xlsXformatGetFontIndex(xf_t* x);
font_t* xlsXformatGetFont(xf_t* x);
/* Format Index wrappers*/
void xlsXformatSetFormat(xf_t* x, format_number_t formatidx);
void xlsXformatSetFormatP(xf_t* x, format_t* fmt);
/* Horizontal Align option wrappers*/
void xlsXformatSetHAlign(xf_t* x, halign_option_t ha_option);
ubyte xlsXformatGetHAlign(xf_t* x);
/* Vertical Align option wrappers*/
void xlsXformatSetVAlign(xf_t* x, valign_option_t va_option);
ubyte xlsXformatGetVAlign(xf_t* x);
/* Indent option wrappers*/
void xlsXformatSetIndent(xf_t* x, indent_option_t in_option);
ubyte xlsXformatGetIndent(xf_t* x);
/* Text orientation option wrappers*/
void xlsXformatSetTxtOrientation(xf_t* x, txtori_option_t ori_option);

ubyte xlsXformatGetTxtOrientation(xf_t* x);
/* Fill Foreground color option wrappers*/
void xlsXformatSetFillFGColor(xf_t* x, color_name_t color);
ushort xlsXformatGetFillFGColorIdx(xf_t* x);
/* Fill Background color option wrappers*/
void xlsXformatSetFillBGColor(xf_t* x, color_name_t color);
ushort xlsXformatGetFillBGColorIdx(xf_t* x);
/* Fill Style option wrappers*/
void xlsXformatSetFillStyle(xf_t* x, fill_option_t fill);
ubyte xlsXformatGetFillStyle(xf_t* x);
/* Locked option wrappers*/
void xlsXformatSetLocked(xf_t* x, bool locked_opt);
bool xlsXformatIsLocked(xf_t* x);
/* Hidden option wrappers*/
void xlsXformatSetHidden(xf_t* x, bool hidden_opt);
bool xlsXformatIsHidden(xf_t* x);
/* Wrap option wrappers*/
void xlsXformatSetWrap(xf_t* x, bool wrap_opt);
bool xlsXformatIsWrap(xf_t* x);
/* Cell option wrappers*/
void xlsXformatSetBorderStyle(xf_t* x, border_side_t side, border_style_t style);

void xlsXformatSetBorderColor(xf_t* x, border_side_t side, color_name_t color);

void xlsXformatSetBorderColorIdx(xf_t* x, border_side_t side, ubyte color);

ubyte xlsXformatGetBorderStyle(xf_t* x, border_side_t side);

ushort xlsXformatGetBorderColorIdx(xf_t* x, border_side_t side);


// Font
void xlsFontSetName(font_t* f, const char* fntname);
char* xlsFontGetName(font_t* f, char* dst, size_t dstsize);
/* FONT height wrappers*/
void xlsFontSetHeight(font_t* f, ushort fntheight);
ushort xlsFontGetHeight(font_t* f);
/* FONT boldstyle wrappers*/
void xlsFontSetBoldStyle(font_t* f, boldness_option_t fntboldness);

ushort xlsFontGetBoldStyle(font_t* f);
/* FONT underline wrappers*/
void xlsFontSetUnderlineStyle(font_t* f, underline_option_t fntunderline);

ubyte xlsFontGetUnderlineStyle(font_t* f);
/* FONT script wrappers*/
void xlsFontSetScriptStyle(font_t* f, script_option_t fntscript);

ushort xlsFontGetScriptStyle(font_t* f);
/* FONT script wrappers*/
void xlsFontSetColor(font_t* f, color_name_t fntcolor);
ushort xlsFontGetColorIdx(font_t* f);
void xlsFontSetItalic(font_t* f, bool italic);
void xlsFontSetStrikeout(font_t* f, bool so);
/* FONT  attributes wrappers */
// Macintosh only
void xlsFontSetOutline(font_t* f, bool ol);
void xlsFontSetShadow(font_t* f, bool sh);

// these are accessing private members. Is this intended?
ushort xlsFontGetAttributes(font_t* f);
version(DEPRECATED)
void xlsFontSetAttributes(font_t* f, ushort attr);
//uint xlsXformatGetSignature(xf_t* x);
bool xlsXformatIsCell(xf_t* x);
void xlsXformatSetCellMode(xf_t* x, bool cellmode);
ushort xlsXformatGetFormatIndex(xf_t* x);
format_number_t xlsXformatGetFormat(xf_t* x);
void xlsCellSetXF(cell_t* c, xf_t* pxfval);
ushort xlsCellGetXFIndex(cell_t* c);
//void xlsCellFontattr(cell_t* c, ushort attr);

// assert_assist:
alias xlslib_userdef_assertion_reporter = typeof(*xlslib_userdef_assertion_reporter_p);

void xlslib_report_failed_assertion(const char* expr, const char* fname, int lineno, const char* funcname);
void xlslib_register_assert_reporter(xlslib_userdef_assertion_reporter* user_func);
