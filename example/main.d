﻿module xls_text;


import core.sys.windows.windows;
import core.stdc.stdlib;
import core.stdc.stdio;

static import std.stdio;

import xlslib.lib;


extern(C) static void my_xlslib_assertion_reporter(const char* expr, const char* filename, int lineno, const char* funcname) nothrow
{
	fprintf(stderr, "Assertion failed: %s at line %d", 
			(expr ? expr : "???"),
			lineno);
	if (funcname)
	{
		fprintf(stderr, " (%s)", funcname);
	}
	if (filename)
	{
		fprintf(stderr, " in %s\n", filename);
	}
	else
	{
		fprintf(stderr, " in [unidentified source file]\n");
	}
	exit(EXIT_FAILURE);
}


void writeUnicodeLabel(worksheet* ws, uint row, uint col)
{
   const wchar_t* latin1wstr = "\x0055\x006e\x0069\x0063\x006f\x0064\x0065\x0020\x0074\x0065\x0078\x0074\x0020\x00e3\x00f5\x00f1\x00e1\x00e9\x00fa\x00ed\x00f3\x002c\x00e0\x00e8\x00ec\x00f2\x00f9\x00e4\x00eb\x00ef\x00f6\x00fc\x00f1\x00e2\x00ea\x00ee\x00f4\x00fb\0"w.ptr;
   const wchar_t* wstr = "\x3042\x3043\0"w.ptr; // 2 Hiragana characters

	xlsWorksheetLabel(ws, row++, col, "Two Japanese Hiragana:", null);
	xlsWorksheetLabelW(ws, row++, col, wstr, null);
	xlsWorksheetLabel(ws, row++, col, "A few words plus a series of Latin1 accented letters:", null);
	xlsWorksheetLabelW(ws, row++, col, latin1wstr, null);
}

int main(string[] args)
{
	xlslib_register_assert_reporter(&my_xlslib_assertion_reporter);

	workbook* w = xlsNewWorkbook();
	worksheet* ws = xlsWorkbookSheet(w, "xlslib C");
	xlsWorksheetNumberDbl(ws, 1, 1, 1.0, null);  
	xlsWorksheetNumberDbl(ws, 2, 1, 2.0, null);
	xlsWorksheetNumberDbl(ws, 3, 1, 3.0, null);
	xlsWorksheetLabel(ws, 4, 1, "ASCII text", null);
	writeUnicodeLabel(ws, 5, 1);
	int ret = xlsWorkbookDump(w, "testC.xls");

	xlsDeleteWorkbook(w);

	if (ret != NO_ERRORS)
	{
		std.stdio.stderr.writefln("%s failed: I/O failure %d.", args[0], ret);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
